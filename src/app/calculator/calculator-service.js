angular.module('app.calculator-service', [

])

.factory('CalculatorService', function(CalculatorApi) {
    var steps = [];

    return {
        addStep: function(id, number) {
            var replaceCount = 0;
            var insertPosition = steps.length;

            if (this.getApplyStepIndex() !== -1) {
                insertPosition--;
                if (id === 'apply') {
                    replaceCount = 1;
                }
            }

            steps.splice(insertPosition, replaceCount, {id: id, number: number});
        },

        getApplyStepIndex: function() {
            return _.findIndex(steps, {id: 'apply'});
        },

        getSteps: function() {
            return steps;
        },

        clearSteps: function() {
            steps = [];
        },

        calculate: function() {
            var result = {};

            if (steps.length === 0) {
                result.status = 'err';
                result.message = 'No steps defined.';
            } else if (this.getApplyStepIndex() === -1) {
                result.status = 'err';
                result.message = 'No apply step defined.';
            } else {
                var operations = CalculatorApi.getOperations();
                var applyStep = steps.slice(-1)[0];
                var value = applyStep.number;

                var i = 0;
                var step = steps[i];
                while (step.id !== 'apply') {
                    value = operations[step.id].action(value, step.number);
                    step = steps[++i];
                }

                result.status = 'ok';
                result.value = value;
            }

            return result;
        }
    };
})

;
