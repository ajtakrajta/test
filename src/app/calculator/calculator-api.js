angular.module('app.calculator-api', [

])

.factory('CalculatorApi', function() {
    var operations = {};

    return {
        addOperation: function(operation) {
            operations[operation.id] = operation;
        },
        getOperations: function() {
            return operations;
        },
        getOperation: function(id) {
            return operations[id] ? operations[id] : undefined;
        }
    };
})

;

