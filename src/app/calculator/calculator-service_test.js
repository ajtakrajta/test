
describe('calculator service', function() {
    var factory, api;

    beforeEach(module('app.calculator-api'));
    beforeEach(module('app.calculator-service'));

    beforeEach(inject(function(CalculatorService, CalculatorApi) {
        factory = CalculatorService;
        api = CalculatorApi;
    }));

    it('should be defined', function() {
        expect(factory).toBeDefined();
    });

    it('should store and retrieve provided steps', function() {
        factory.addStep('id1', 1);
        factory.addStep('id2', 2);
        expect(factory.getSteps().length).toEqual(2);
    });

    it('should keep only one apply step', function() {
        factory.addStep('apply', 1);
        factory.addStep('apply', 2);
        expect(factory.getSteps().length).toEqual(1);
        expect(factory.getSteps()[0].id).toEqual('apply');
        expect(factory.getSteps()[0].number).toEqual(2);
    });

    it('should keep apply at the end of steps list', function() {
        factory.addStep('apply', 1);
        factory.addStep('add', 1);
        expect(factory.getSteps().length).toEqual(2);
        expect(factory.getSteps()[0].id).toEqual('add');
        expect(factory.getSteps()[1].id).toEqual('apply');
    });

    it('should find apply step in the steps list', function() {
        expect(factory.getApplyStepIndex()).toEqual(-1);
        factory.addStep('apply', 1);
        expect(factory.getApplyStepIndex()).toEqual(0);
        factory.addStep('add', 1);
        expect(factory.getApplyStepIndex()).toEqual(1);
    });

    it('should be able to clear steps list', function() {
        factory.addStep('apply', 1);
        expect(factory.getSteps().length).toEqual(1);
        factory.clearSteps();
        expect(factory.getSteps().length).toEqual(0);
    });

    it('should return error when calculating result of empty steps list', function() {
        var result = factory.calculate();
        expect(result.status).toEqual('err');
    });

    it('should return error when calculating result of steps with no apply step', function() {
        factory.addStep('add', 1);
        var result = factory.calculate();
        expect(result.status).toEqual('err');
    });

    var addTestOperations = function() {
        api.addOperation({
            id: '+', name: '+',
            action: function(num1, num2) { return num1 + num2; }
        });

        api.addOperation({
            id: '*', name: '*',
            action: function(num1, num2) { return num1 * num2; }
        });
    };

    it('should compute correct result of steps +2, *3, apply 3', function() {
        addTestOperations();
        factory.addStep('+', 2);
        factory.addStep('*', 3);
        factory.addStep('apply', 3);
        var result = factory.calculate();
        expect(result.status).toEqual('ok');
        expect(result.value).toEqual(15);
    });

    it('should compute correct result of steps apply 5, *9', function() {
        addTestOperations();
        factory.addStep('apply', 5);
        factory.addStep('*', 9);
        var result = factory.calculate();
        expect(result.status).toEqual('ok');
        expect(result.value).toEqual(45);
    });

    it('should compute correct result of steps apply 1', function() {
        addTestOperations();
        factory.addStep('apply', 1);
        var result = factory.calculate();
        expect(result.status).toEqual('ok');
        expect(result.value).toEqual(1);
    });

});

