
describe('calculator multiply operation', function() {
    var operation;

    beforeEach(module('app.calculator-api'));

    beforeEach(inject(function(CalculatorOperationMultiply) {
        operation = CalculatorOperationMultiply;
    }));

    it('should be defined', function() {
        expect(operation).toBeDefined();
    });

    it('should contains id, name and action properties', function() {
        expect(operation.id).toBeDefined();
        expect(operation.name).toBeDefined();
        expect(operation.action).toBeDefined();
    });

    it('should provide operation multiplying two numbers', function() {
        expect(operation.action(2, 3)).toEqual(6);
    });

});
