
angular.module('app.calculator-api')

.run(function(CalculatorApi, CalculatorOperationMultiply) {
    CalculatorApi.addOperation(CalculatorOperationMultiply);
})

.factory('CalculatorOperationMultiply', function() {
    return {
        id: 'multiply',
        name: 'Multiply',
        action: function(num1, num2) {
            return num1 * num2;
        }
    };
})

;


