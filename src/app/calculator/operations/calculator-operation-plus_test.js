
describe('calculator plus operation', function() {
    var operation;

    beforeEach(module('app.calculator-api'));

    beforeEach(inject(function(CalculatorOperationPlus) {
        operation = CalculatorOperationPlus;
    }));

    it('should be defined', function() {
        expect(operation).toBeDefined();
    });

    it('should contains id, name and action properties', function() {
        expect(operation.id).toBeDefined();
        expect(operation.name).toBeDefined();
        expect(operation.action).toBeDefined();
    });

    it('should provide operation adding two numbers', function() {
        expect(operation.action(1, 2)).toEqual(3);
    });

});
