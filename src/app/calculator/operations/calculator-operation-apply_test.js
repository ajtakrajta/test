
describe('calculator apply operation', function() {
    var operation;

    beforeEach(module('app.calculator-api'));

    beforeEach(inject(function(CalculatorOperationApply) {
        operation = CalculatorOperationApply;
    }));

    it('should be defined', function() {
        expect(operation).toBeDefined();
    });

    it('should contains id, name and action properties', function() {
        expect(operation.id).toBeDefined();
        expect(operation.name).toBeDefined();
        expect(operation.action).toBeDefined();
    });

    it('should provide operation setting the initial value', function() {
        expect(operation.action(1)).toEqual(1);
    });

});
