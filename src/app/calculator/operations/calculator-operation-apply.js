
angular.module('app.calculator-api')

.run(function(CalculatorApi, CalculatorOperationApply) {
    CalculatorApi.addOperation(CalculatorOperationApply);
})

.factory('CalculatorOperationApply', function() {
    return {
        id: 'apply',
        name: 'Apply',
        action: function(num1) {
            return num1;
        }
    };
})

;

