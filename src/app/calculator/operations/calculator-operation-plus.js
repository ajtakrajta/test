
angular.module('app.calculator-api')

.run(function(CalculatorApi, CalculatorOperationPlus) {
    CalculatorApi.addOperation(CalculatorOperationPlus);
})

.factory('CalculatorOperationPlus', function() {
    return {
        id: 'add',
        name: 'Add',
        action: function(num1, num2) {
            return num1 + num2;
        }
    };
})

;


