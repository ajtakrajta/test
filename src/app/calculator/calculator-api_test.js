
describe('calculator api', function() {
    var factory;

    beforeEach(module('app.calculator-api'));

    beforeEach(inject(function(CalculatorApi) {
        factory = CalculatorApi;
    }));

    it('should be defined', function() {
        expect(factory).toBeDefined();
    });

    it('should store provided operations', function() {
        factory.addOperation({ id: 'op1' });
        factory.addOperation({ id: 'op2' });
        expect(factory.getOperations()['op1']).toBeDefined();
        expect(factory.getOperations()['op2']).toBeDefined();
    });

    it('should provide stored operations by id', function() {
        factory.addOperation({id: 'op1'});
        expect(factory.getOperation('op1')).toBeDefined();
        expect(factory.getOperation('opX')).not.toBeDefined();
    });

});

