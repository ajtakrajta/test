
angular.module('app.calculator', [
    'ui.router',
    'app.calculator-api',
    'app.calculator-service'
])

.config(function($stateProvider) {
    $stateProvider.state('app.calculator', {
        url: '/',
        views: {
            "main": {
                controller: 'CalculatorCtrl',
                templateUrl: 'calculator/calculator.html'
            }
        },
        data: {
            pageTitle: 'Home'
        }
    });
})

.controller('CalculatorCtrl', function($scope, CalculatorApi, CalculatorService, $timeout) {
    $scope.operations = CalculatorApi.getOperations();

    $scope.form = {};
    $scope.steps = [];

    var RESULT_NOT_AVAILABLE = {
        value: 'N/A',
        message: null
    };

    $scope.result = RESULT_NOT_AVAILABLE;

    $scope.addStep = function(ev) {
        ev.preventDefault();

        CalculatorService.addStep($scope.form.operation, $scope.form.number);
        $scope.steps = CalculatorService.getSteps();
    };

    $scope.reset = function(ev) {
        ev.preventDefault();

        $scope.steps = [];
        $scope.form = {};
        $scope.result = RESULT_NOT_AVAILABLE;

        CalculatorService.clearSteps();
    };

    $scope.calculate = function(ev) {
        ev.preventDefault();

        var calculation = CalculatorService.calculate();
        var result = {};

        if (calculation.status === 'err') {
            result.value = RESULT_NOT_AVAILABLE.value;
            result.message = calculation.message;
            $timeout(function() {
                $scope.result.message = '';
            }, 2000);
        } else {
            result.value = calculation.value;
            result.message = '';
        }

        $scope.result = result;
    };
})

;

