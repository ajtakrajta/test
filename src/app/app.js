
angular.module('app', [
    'ui.router',
    'templates-app',
    'templates-common',
    'app.calculator'
])

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider.state('app', {
        abstract: true,
        views: {
            "main": {
                controller: 'AppCtrl',
                template: '<div ui-view="main"></div>'
            }
        }
    });

    $urlRouterProvider.otherwise('/');
})

.run(function() {

})

.controller('AppCtrl', function AppCtrl($scope, $location) {
    $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        if (angular.isDefined(toState.data.pageTitle)) {
            $scope.pageTitle = toState.data.pageTitle + ' | cng-calculator' ;
        }
    });
})

;

